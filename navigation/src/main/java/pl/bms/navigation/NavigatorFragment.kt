package pl.bms.navigation

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
abstract class NavigatorFragment : Fragment(){

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        ResultListenerBus.emitEvent(ResultEvent.Fragment(this, requestCode, resultCode, intent))
        super.onActivityResult(requestCode, resultCode, intent)
    }

    fun navigate(
            activityEntry: ActivityEntry,
            modifyIntent: (Intent.() -> Unit)? = null,
            onResult: ((capturingFragment: NavigatorFragment, resultCode: Int, data: Intent?) -> Unit)? = null) {
        val intent = Intent(activity, activityEntry.type)
                .apply( activityEntry.putToIntent() )
                .apply { modifyIntent?.let { it() } }
        if (onResult != null) {
            val requestId = RequestIdCreator.create()
            NavigatorListeners.fragmentListeners[requestId] = onResult
            startActivityForResult(intent, requestId)
        } else {
            startActivity(intent)
        }
    }

}