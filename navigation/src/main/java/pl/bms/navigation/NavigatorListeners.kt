package pl.bms.navigation

import android.content.Intent
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Copyright (c) 2018, BM Solution & Soft-AB.
 * Created by Rafał Olszewski on 19.01.18.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See
 * the License for the specific language governing permissions and limitations under the License.
 *
 */
internal object NavigatorListeners {

    val activityListeners = mutableMapOf<Int, (NavigatorActivity.(resultCode: Int, data: Intent?) -> Unit)>()
    val fragmentListeners = mutableMapOf<Int, (NavigatorFragment.(resultCode: Int, data: Intent?) -> Unit)>()

    init {
        ResultListenerBus.observe()
                .observeOn( AndroidSchedulers.mainThread())
                .subscribe{event ->
                    when(event) {
                        is ResultEvent.Activity -> {
                            activityListeners[event.requestId]?.invoke(event.activity, event.resultCode, event.intent)
                            activityListeners.remove(event.requestId)
                        }
                        is ResultEvent.Fragment -> {
                            fragmentListeners[event.requestId]?.invoke(event.fragment, event.resultCode, event.intent)
                            fragmentListeners.remove(event.requestId)
                        }
                    }

                }
    }

}

