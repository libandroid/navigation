package pl.bms.navigationlib

import android.content.Intent
import pl.bms.navigation.ActivityEntry


/**
 * Created by Rafał Olszewski on 16.02.18.
 *
 */
class WithParamsActivityEntry(
        val intParameter: Int,
        val optionalParameter: String?
): ActivityEntry {

    override val type = WithParamsActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {
        putExtra(PARAM_INT, intParameter)
        putExtra(PARAM_OPTIONAL, optionalParameter)
    }

    companion object {
        const val PARAM_INT = "param.int"
        const val PARAM_OPTIONAL = "param.optional"
    }
}