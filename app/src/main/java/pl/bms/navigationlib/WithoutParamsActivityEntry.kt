package pl.bms.navigationlib

import android.content.Intent
import pl.bms.navigation.ActivityEntry


/**
 * Created by Rafał Olszewski on 16.02.18.
 *
 */
class WithoutParamsActivityEntry: ActivityEntry {

    override val type = WithoutParamsActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {}

}