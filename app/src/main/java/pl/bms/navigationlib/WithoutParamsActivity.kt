package pl.bms.navigationlib

import android.os.Bundle
import pl.bms.navigation.NavigatorActivity

class WithoutParamsActivity : NavigatorActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_without_params)

    }
}
