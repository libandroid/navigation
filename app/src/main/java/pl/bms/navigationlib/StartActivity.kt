package pl.bms.navigationlib

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_start.*
import pl.bms.navigation.NavigatorActivity
import java.util.*

class StartActivity : NavigatorActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        withoutParamsButton.setOnClickListener {

            //Navigate without params:
            navigate(WithoutParamsActivityEntry())

        }

        withParamsButton.setOnClickListener {
            try {
                val stringParameter = getStringFromEditText()
                val intParameter = intEditText.text.toString().toInt()

                // Navigate with params
                navigate(WithParamsActivityEntry(intParameter, stringParameter))

            } catch (e: NumberFormatException) {
                showToast("Enter int value")
            }

        }

        withResultButton.setOnClickListener {

            val randomInt = Random().nextInt(1000)

            navigate(WithResultActivityEntry(randomInt)) { resultCode, data ->
                if (resultCode == Activity.RESULT_OK) {
                    val resultString = data?.getString(WithResultActivity.RESULT_STRING) ?: "no result"
                    resultTextView.text = resultString
                } else if (resultCode == Activity.RESULT_CANCELED){
                    showToast("result canceled")
                }
            }

        }

    }

    private fun showToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    }

    private fun getStringFromEditText(): String? {
        val stringParameter =
                if (stringEditText.text.toString().isEmpty()) {
                    null
                } else {
                    stringEditText.text.toString()
                }
        return stringParameter
    }
}
