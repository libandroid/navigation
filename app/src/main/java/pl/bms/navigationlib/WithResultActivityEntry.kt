package pl.bms.navigationlib

import android.content.Intent
import pl.bms.navigation.ActivityEntry


/**
 * Created by Rafał Olszewski on 16.02.18.
 *
 */
class WithResultActivityEntry(val randomInt: Int): ActivityEntry {

    override val type = WithResultActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {
        putExtra(PARAM, randomInt)
    }

    constructor(intent: Intent) : this(
            intent.getIntExtra(PARAM, -1)
    )

    companion object {
        private const val PARAM = "param"
    }
}