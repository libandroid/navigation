package pl.bms.navigationlib

import android.os.Bundle
import pl.bms.navigation.NavigatorActivity
import kotlinx.android.synthetic.main.activity_with_params.*

class WithParamsActivity : NavigatorActivity() {

    private val intParam: Int by lazy { intent.getIntExtra(WithParamsActivityEntry.PARAM_INT, -1) }
    private val optionalParam: String? by lazy { intent.getStringExtra(WithParamsActivityEntry.PARAM_OPTIONAL) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_with_params)

        stringValueTextView.text = optionalParam ?: "null"

        intValueTextView.text = intParam.toString()

    }
}
