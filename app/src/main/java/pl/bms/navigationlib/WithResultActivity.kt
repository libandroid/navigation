package pl.bms.navigationlib

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import pl.bms.navigation.NavigatorActivity
import kotlinx.android.synthetic.main.activity_with_result.*

class WithResultActivity : NavigatorActivity() {

    companion object {
        const val RESULT_STRING = "result"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_with_result)

        val entry = WithResultActivityEntry(intent)

        randomIntParamTextView.text = entry.randomInt.toString()

        resultButton.setOnClickListener {
            setResult(Activity.RESULT_OK, Intent().apply { putExtra(RESULT_STRING, resultEditText.text.toString()) })
            finish()
        }

    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}
