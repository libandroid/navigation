# Navigation

Android library for navigation between activities. Written in kotlin

Current version: `0.5`

```groovy
implementation 'pl.bms:navigation:0.5'
```
minSdkVersion **15**

### Usage

Simply go to another activity:
```kotlin
navigate(SomeActivityEntry())
```

If you want to add some parameters to activity:
```kotlin
navigate(SomeActivityEntry(firstParameter, secondParameter))
```

if you want to do something with result:
```kotlin
navigate(SomeActivityEntry(firstParameter, secondParameter)) { resultCode, data ->
    if (resultCode == Activity.RESULT_OK) {
		// Do something
    }
}
```

### Setup ActivityEntry
To achive this, you need to create class which extands `ActivityEntry` for every activity

```kotlin
class SomeActivityEntry : ActivityEntry {

    override val type = SomeActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {}
    
}
```
You must override :
1. **type** with the class you want to navigate to
2. **putToIntent()** set parameters to intent

### Example with parameters:

```kotlin
class SomeActivityEntry(
	val intParameter: Int,
	val optionalParameter: String?,
	val parclelableParameter: ParcelableClass
) : ActivityEntry {

    override val type = SomeActivity::class.java

    override fun putToIntent(): Intent.() -> Unit = {
		putExtra(PARAM_INT, intParameter)
		putExtra(PARAM_OPTIONAL, optionalParameter)
		putExtra(PARAM_PARCELABLE, parclelableParameter)
	}
	
	companion object {
        const val PARAM_INT = "param.int"
        const val PARAM_OPTIONAL = "param.optinal"
        const val PARAM_PARCELABLE = "param.parcelable"
    }
}
```
So you can get parameters from activity like this:
```kotlin
val intParameter = intent.getIntExtra(SomeActivityEntry.PARAM_INT, -1) 
val optionalParameter: String? = intent.getStringExtra(SomeActivityEntry.PARAM_OPTIONAL) 
val parclelableParameter = intent.getParcelableExtra<ParcelableClass>(SomeActivityEntry.PARAM_PARCELABLE) 
```

### Secondary constructor in ActivityEntry

You can also add secondary constructor with intent as parameter to ActivityEntry:

```kotlin
constructor(intent: Intent) : this(
        intent.getIntExtra(PARAM_INT, -1),
        intent.getStringExtra(PARAM_OPTIONAL),
        intent.getParcelableExtra<ParcelableClass>(PARAM_PARCELABLE)
)
```
So you can create entry in Activity, and use all parameters:
```kotlin
val entry = SomeActivityEntry(intent) 
entry.intParameter
entry.optionalParameter
entry.parclelableParameter
```


### Used dependecies

	"org.jetbrains.kotlin:kotlin-stdlib-jre7:$kotlin_version"
    "com.android.support:appcompat-v7:$support_version"
    "io.reactivex.rxjava2:rxjava:$rx_version"
    "io.reactivex.rxjava2:rxandroid:$rxandroid_version"

## Dependecies verions 

```groovy
	kotlin_version = '1.2.41'
    support_version = '27.1.1'
    rx_version = '2.1.8'
    rxandroid_version = '2.0.1'
```

## License
	Copyright 2018 BM Solution & Soft-AB

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	   http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
